from github3 import GitHub
from datetime import datetime, timezone
from argparse import ArgumentParser
import sys


token = "*****"


def parse_args():
    p = ArgumentParser()
    g = p.add_mutually_exclusive_group(required=True)
    g.add_argument(
        "--project-number", type=int, dest="number", help="GitHub project number"
    )
    g.add_argument(
        "--project-name",
        type=str,
        dest="name",
        help="GitHub project name, or part of it",
    )
    return p.parse_args()


def format_solved_issue(number: int, title: str, url: str) -> str:
    return f"* [#{number}]({url}) - {title}"


def print_fmtted_issues():
    repo = GitHub(token=token).repository("root-project", "root")
    args = parse_args()

    def matches(project):
        return project.number == args.number or (
            args.name is not None and args.name in project.name
        )

    pro = [p for p in repo.projects() if matches(p)]
    if len(pro) != 1:
        print(
            f"Could not identify a unique GitHub project in root-project/root with number "
            + f"{args.number or '*'} and name containing {args.name or '*'}",
            file=sys.stderr,
        )
        sys.exit(1)
    pro = pro[0]

    col = list(pro.columns())
    if len(col) != 1:
        print(f"Project '{pro.name}' has more than one column", file=sys.stderr)
        sys.exit(2)
    col = col[0]

    issues = [card.retrieve_issue_from_content() for card in col.cards()]
    fmtted_issues = [format_solved_issue(i.number, i.title, i.html_url) for i in issues if i.pull_request() is None]
    print(pro.name)
    for i in fmtted_issues:
        print(i)

    prs = [format_solved_issue(i.number, i.title, i.html_url) for i in issues if i.pull_request() is not None]
    # check we did not miss anything and we did not count anything twice
    assert(len(issues) == len(fmtted_issues) + len(prs))
    if len(prs) > 0:
        print("<!--")
        print("Also found the following pull requests in this project (they should not be there):")
        for pr in prs:
            print(pr)
        print("-->")


if __name__ == "__main__":
    print_fmtted_issues()
